import org.kaaproject.kaa.client.DesktopKaaPlatformContext;
import org.kaaproject.kaa.client.Kaa;
import org.kaaproject.kaa.client.KaaClient;
import org.kaaproject.kaa.client.SimpleKaaClientStateListener;
import org.kaaproject.kaa.client.configuration.base.ConfigurationListener;
import org.kaaproject.kaa.client.configuration.base.SimpleConfigurationStorage;
import org.kaaproject.kaa.client.logging.strategies.RecordCountLogUploadStrategy;
import org.kaaproject.kaa.schema.smart_printing_machine_iot.Configuration;
import org.kaaproject.kaa.schema.smart_printing_machine_iot.DataCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Random;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Class implement functionality for Smart Printer application. Application send Printer data
 * from the Kaa endpoint with required configured sampling period
 */
public class SmartPrintingMachine {

    private static final long DEFAULT_START_DELAY = 2000L;
    private static int DEFAULT_TONER_SIZE = 2000;
    private static int cTonerLevel = 2000;
    private static int totalPages = 400;
    private static int remainingPages = 400;
    private static int cPagesPrinted = 0;
    private static int totalPagesPrinted = 0;
    private static long timestamp = 0;

    private static final Logger LOG = LoggerFactory.getLogger(SmartPrintingMachine.class);

    private static KaaClient kaaClient;

    private static ScheduledFuture<?> scheduledFuture;
    private static ScheduledExecutorService scheduledExecutorService;

    public static void main(String[] args) {
        LOG.info(SmartPrintingMachine.class.getSimpleName() + " app starting!");

        scheduledExecutorService = Executors.newScheduledThreadPool(1);

        //Create the Kaa desktop context for the application.
        DesktopKaaPlatformContext desktopKaaPlatformContext = new DesktopKaaPlatformContext();
        /*
         * Create a Kaa client and add a listener which displays the Kaa client
         * configuration as soon as the Kaa client is started.
         */
        kaaClient = Kaa.newClient(desktopKaaPlatformContext, new PrintingMachineClientStateListener(), true);

        /*
         *  Used by log collector on each adding of the new log record in order to check whether to send logs to server.
         *  Start log upload when there is at least one record in storage.
         */
        RecordCountLogUploadStrategy strategy = new RecordCountLogUploadStrategy(1);
        strategy.setMaxParallelUploads(1);
        kaaClient.setLogUploadStrategy(strategy);

        /*
         * Persist configuration in a local storage to avoid downloading it each
         * time the Kaa client is started.
         */
        kaaClient.setConfigurationStorage(new SimpleConfigurationStorage(desktopKaaPlatformContext, "saved_config.cfg"));

        kaaClient.addConfigurationListener(new ConfigurationListener() {
            @Override
            public void onConfigurationUpdate(Configuration configuration) {
                LOG.info("**************************************************");
                LOG.info("Received configuration data. New sample period: {}", configuration.getSamplePeriod());
                LOG.info("**************************************************");
                onChangedConfiguration(TimeUnit.SECONDS.toMillis(configuration.getSamplePeriod()));
            }
        });

        //Start the Kaa client and connect it to the Kaa server.
        kaaClient.start();

        LOG.info("--= Press any key to exit =--");
        try {
            System.in.read();
        } catch (IOException e) {
            LOG.error("IOException has occurred: {}", e.getMessage());
        }
        LOG.info("Stopping...");
        scheduledExecutorService.shutdown();
        kaaClient.stop();
    }

    /* ****RANDOM DATA EMULATION METHOD****
     * Method, that emulate random printing machine data.
     * Retrieves random printing data object.
     */
    private static PrintingData getPrinterDataRand() {
        //return an object of model class with all random values
        Random rd = new Random();

        //#1. Value for toner level
        if (cTonerLevel > 0) {
            int inkUsed = ((rd.nextInt(6) * 2) + (rd.nextInt(6) * 3));
            if (inkUsed < cTonerLevel) {
                cTonerLevel -= inkUsed;
            } else {   //log here for empty values
                inkUsed = cTonerLevel;//give all the ink you got and refill the toner
                cTonerLevel = 2000;
            }
        }

        //#2. Value for DEFAULT_TONER_SIZE
        //#3. Value for printed pages
        //#4. Value for total pages printed
        //#5. Value Total Pages (Default Size of Page Container)
        //#6. Value for remaning pages
        if (remainingPages > 0) {
            //#3. Value for printed pages
            cPagesPrinted = ((rd.nextInt(6) * 2) + (rd.nextInt(2) * 3)) + 1;
            if (cPagesPrinted < remainingPages) {
                //#6. Value for remaning pages
                remainingPages -= cPagesPrinted;
                //#4. Value for total pages printed
                totalPagesPrinted += cPagesPrinted; //add currently printed pages to the total printed pages
            } else {
                cPagesPrinted = remainingPages;//give all the pages you got and refill the page container
                totalPagesPrinted += cPagesPrinted; //add currently printed pages to the total printed pages
                remainingPages = 400;
            }
        }
        //#7.Generate timestamp here
        timestamp = System.currentTimeMillis();

        //#8. for days of week random**
        String[] days = new String[7];
        days[0] = "Sunday";
        days[1] = "Monday";
        days[2] = "Tuesday";
        days[3] = "Wednesday";
        days[4] = "Thursday";
        days[5] = "Friday";
        days[6] = "Saturday";
        String dayName = days[rd.nextInt(7)];

        //#9. for months random**
        String[] months = new String[12];
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";
        String monthName = months[rd.nextInt(12)];

        Date date = new Date();  //current date
        //#10. for current date
        SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
        String dateValue = dfDate.format(date);

        //#11. for time fetched from current date
        SimpleDateFormat dfTime = new SimpleDateFormat("HH:mm");
        String timeValue = dfTime.format(date);

        return new PrintingData(cTonerLevel, DEFAULT_TONER_SIZE, cPagesPrinted, totalPagesPrinted, totalPages, remainingPages, timestamp, dayName, monthName, dateValue, timeValue);
    }

    private static void onKaaStarted(long time) {
        if (time <= 0) {
            LOG.error("Wrong time is used. Please, check your configuration!");
            kaaClient.stop();
            System.exit(0);
        }

        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        PrintingData pData = getPrinterDataRand();
                        kaaClient.addLogRecord(new DataCollection(pData.getcTonerLevel(), pData.getTotalTonerSize(), pData.getcPagesPrinted(), pData.getTotalPagesPrinted(), pData.getTotalPages(), pData.getRemainingPages(), pData.getTimeStamp(), pData.getDayName(), pData.getMonthName(), pData.getDateValue(), pData.getTimeValue()));
                        LOG.info("Current Toner Level: {}", pData.getcTonerLevel());
                        LOG.info("Total Toner Size: {}", pData.getTotalTonerSize());
                        LOG.info("Pages Printed Now: {}", pData.getcPagesPrinted());
                        LOG.info("Total Pages Printed: {}", pData.getTotalPagesPrinted());
                        LOG.info("Total Pages: {}", pData.getTotalPages());
                        LOG.info("Remaining Pages: {}", pData.getRemainingPages());
                        LOG.info("Timestamp: {}", pData.getTimeStamp());
                        LOG.info("Day: {}", pData.getDayName());
                        LOG.info("Month: {}", pData.getMonthName());
                        LOG.info("Date: {}", pData.getDateValue());
                        LOG.info("Time: {}", pData.getTimeValue());
                    }
                }, 0, time, TimeUnit.MILLISECONDS);
    }

    private static void onChangedConfiguration(long time) {
        if (time == 0) {
            time = DEFAULT_START_DELAY;
        }
        scheduledFuture.cancel(false);

        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        PrintingData pData = getPrinterDataRand();
                        kaaClient.addLogRecord(new DataCollection(pData.getcTonerLevel(), pData.getTotalTonerSize(), pData.getcPagesPrinted(), pData.getTotalPagesPrinted(), pData.getTotalPages(), pData.getRemainingPages(), pData.getTimeStamp(), pData.getDayName(), pData.getMonthName(), pData.getDateValue(), pData.getTimeValue()));
                        LOG.info("Current Toner Level: {}", pData.getcTonerLevel());
                        LOG.info("Total Toner Size: {}", pData.getTotalTonerSize());
                        LOG.info("Pages Printed Now: {}", pData.getcPagesPrinted());
                        LOG.info("Total Pages Printed: {}", pData.getTotalPagesPrinted());
                        LOG.info("Total Pages: {}", pData.getTotalPages());
                        LOG.info("Remaining Pages: {}", pData.getRemainingPages());
                        LOG.info("Timestamp: {}", pData.getTimeStamp());
                        LOG.info("Day: {}", pData.getDayName());
                        LOG.info("Month: {}", pData.getMonthName());
                        LOG.info("Date: {}", pData.getDateValue());
                        LOG.info("Time: {}", pData.getTimeValue());
                    }
                }, 0, time, TimeUnit.MILLISECONDS);
    }

    private static class PrintingMachineClientStateListener extends SimpleKaaClientStateListener {

        @Override
        public void onStarted() {
            super.onStarted();
            LOG.info("Kaa client started");
            Configuration configuration = kaaClient.getConfiguration();
            LOG.info("Default sample period: {}", configuration.getSamplePeriod());
            onKaaStarted(TimeUnit.SECONDS.toMillis(configuration.getSamplePeriod()));
        }

        @Override
        public void onStopped() {
            super.onStopped();
            LOG.info("Kaa client stopped");
        }
    }

    // Model POJO Class for binding
    private static class PrintingData {

        private int cTonerLevel, totalTonerSize, cPagesPrinted, totalPagesPrinted, totalPages, remainingPages;
        private long timeStamp;
        private String dayName, monthName, dateValue, timeValue;

        public PrintingData() {
        }

        public PrintingData(int cTonerLevel, int totalTonerSize, int cPagesPrinted, int totalPagesPrinted, int totalPages, int remainingPages, long timeStamp, String dayName, String monthName, String dateValue, String timeValue) {
            this.cTonerLevel = cTonerLevel;
            this.totalTonerSize = totalTonerSize;
            this.cPagesPrinted = cPagesPrinted;
            this.totalPagesPrinted = totalPagesPrinted;
            this.totalPages = totalPages;
            this.remainingPages = remainingPages;
            this.timeStamp = timeStamp;
            this.dayName = dayName;
            this.monthName = monthName;
            this.dateValue = dateValue;
            this.timeValue = timeValue;
        }

        public int getcTonerLevel() {
            return cTonerLevel;
        }

        public void setcTonerLevel(int cTonerLevel) {
            this.cTonerLevel = cTonerLevel;
        }

        public int getTotalTonerSize() {
            return totalTonerSize;
        }

        public void setTotalTonerSize(int totalTonerSize) {
            this.totalTonerSize = totalTonerSize;
        }

        public int getcPagesPrinted() {
            return cPagesPrinted;
        }

        public void setcPagesPrinted(int cPagesPrinted) {
            this.cPagesPrinted = cPagesPrinted;
        }

        public int getTotalPagesPrinted() {
            return totalPagesPrinted;
        }

        public void setTotalPagesPrinted(int totalPagesPrinted) {
            this.totalPagesPrinted = totalPagesPrinted;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getRemainingPages() {
            return remainingPages;
        }

        public void setRemainingPages(int remainingPages) {
            this.remainingPages = remainingPages;
        }

        public long getTimeStamp() {
            return timeStamp;
        }

        public void setTimeStamp(long timeStamp) {
            this.timeStamp = timeStamp;
        }

        public String getDayName() {
            return dayName;
        }

        public void setDayName(String dayName) {
            this.dayName = dayName;
        }

        public String getMonthName() {
            return monthName;
        }

        public void setMonthName(String monthName) {
            this.monthName = monthName;
        }

        public String getDateValue() {
            return dateValue;
        }

        public void setDateValue(String dateValue) {
            this.dateValue = dateValue;
        }

        public String getTimeValue() {
            return timeValue;
        }

        public void setTimeValue(String timeValue) {
            this.timeValue = timeValue;
        }
    }

}