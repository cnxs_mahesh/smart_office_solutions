import org.kaaproject.kaa.client.DesktopKaaPlatformContext;
import org.kaaproject.kaa.client.Kaa;
import org.kaaproject.kaa.client.KaaClient;
import org.kaaproject.kaa.client.SimpleKaaClientStateListener;
import org.kaaproject.kaa.client.configuration.base.ConfigurationListener;
import org.kaaproject.kaa.client.configuration.base.SimpleConfigurationStorage;
import org.kaaproject.kaa.client.logging.strategies.RecordCountLogUploadStrategy;
import org.kaaproject.kaa.schema.smart_coffee_vm.Configuration;
import org.kaaproject.kaa.schema.smart_coffee_vm.DataCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Random;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Class implement functionality for Smart Coffee Vending Machine application. Application send Coffee Machine related data
 * from the Kaa endpoint with required configured sampling period
 */
public class SmartCoffeeVendingMachine {

    private static final long DEFAULT_START_DELAY = 2000L;
    //default values for all fields Base Values
    private static int MILK_CONTAINER_SIZE = 15000; //default size of container
    private static int WATER_CONTAINER_SIZE = 20000;
    private static int COFFEE_CONTAINER_SIZE = 4000;
    private static int cMilkLevel = 15000; //current level
    private static int cWaterLevel = 20000;
    private static int cCoffeeLevel = 4000;
    private static int cCoffeeDispensed = 0; //coffee dispensed cups currently during two time durations
    private static int totalCoffeeDispensed = 0; //total coffee cups dispensed
    private static long timestamp = 0;

    private static final Logger LOG = LoggerFactory.getLogger(SmartCoffeeVendingMachine.class);
    private static KaaClient kaaClient;
    private static ScheduledFuture<?> scheduledFuture;
    private static ScheduledExecutorService scheduledExecutorService;

    public static void main(String[] args) {
        LOG.info(SmartCoffeeVendingMachine.class.getSimpleName() + " app starting!");

        scheduledExecutorService = Executors.newScheduledThreadPool(1);

        //Create the Kaa desktop context for the application.
        DesktopKaaPlatformContext desktopKaaPlatformContext = new DesktopKaaPlatformContext();

        /*
         * Create a Kaa client and add a listener which displays the Kaa client
         * configuration as soon as the Kaa client is started.
         */
        kaaClient = Kaa.newClient(desktopKaaPlatformContext, new CoffeeMachineClientStateListener(), true);

        /*
         *  Used by log collector on each adding of the new log record in order to check whether to send logs to server.
         *  Start log upload when there is at least one record in storage.
         */
        RecordCountLogUploadStrategy strategy = new RecordCountLogUploadStrategy(1);
        strategy.setMaxParallelUploads(1);
        kaaClient.setLogUploadStrategy(strategy);

        /*
         * Persist configuration in a local storage to avoid downloading it each
         * time the Kaa client is started.
         */
        kaaClient.setConfigurationStorage(new SimpleConfigurationStorage(desktopKaaPlatformContext, "saved_config.cfg"));

        kaaClient.addConfigurationListener(new ConfigurationListener() {
            @Override
            public void onConfigurationUpdate(Configuration configuration) {
                LOG.info("**************************************************");
                LOG.info("Received configuration data. New sample period: {}", configuration.getSamplePeriod());
                // LOG.info("Received configuration data. New sample period: {}", configuration.getSamplePeriod()); //change it to test milkFilled()
                LOG.info("**************************************************");
                onChangedConfiguration(TimeUnit.SECONDS.toMillis(configuration.getSamplePeriod()));
            }
        });

        //Start the Kaa client and connect it to the Kaa server.
        kaaClient.start();

        LOG.info("--= Press any key to exit =--");
        try {
            System.in.read();
        } catch (IOException e) {
            LOG.error("IOException has occurred: {}", e.getMessage());
        }
        LOG.info("Stopping...");
        scheduledExecutorService.shutdown();
        kaaClient.stop();
    }

    /* ****RANDOM DATA EMULATION METHOD****
     * Method, that emulates random coffee vm data.
     * Retrieves random coffee data object.
     */
    private static CoffeeData getCoffeeVMDataRand() {
        //return an object of model class with all random values
        Random rd = new Random();

        //#1. value for milk level 15,000
        if (cMilkLevel > 0) {
            int milkConsumed = ((rd.nextInt(11) * 25) + (rd.nextInt(20) * 5)) + 10;
            if (milkConsumed < cMilkLevel) {
                cMilkLevel -= milkConsumed;
            } else {
                milkConsumed = cMilkLevel;//give all the milk you got and refill the milk container
                cMilkLevel = 15000;
            }
        }

        //#2. MILK_CONTAINER_SIZE
        //#3. value for water level 20,000
        if (cWaterLevel > 0) {
            int waterConsumed = ((rd.nextInt(11) * 25) + (rd.nextInt(20) * 5)) + 10;
            if (waterConsumed < cWaterLevel) {
                cWaterLevel -= waterConsumed;
            } else {   //log here for empty values
                waterConsumed = cWaterLevel;//give all the water you got and refill the water container
                cWaterLevel = 20000;
            }
        }

        //#4. WATER_CONTAINER_SIZE
        //#5. value for coffee level 4,000
        if (cCoffeeLevel > 0) {
            int coffeeConsumed = ((rd.nextInt(20) * 2) + (rd.nextInt(4) * 5)) + 5;
            if (coffeeConsumed < cCoffeeLevel) {
                cCoffeeLevel -= coffeeConsumed;
            } else {   //log here for empty values
                coffeeConsumed = cCoffeeLevel;//give all the milk you got and refill the milk container
                cCoffeeLevel = 4000;
            }
        }

        //#6. COFFEE_CONTAINER_SIZE
        //#7.  random values for coffee dispensed
        cCoffeeDispensed = rd.nextInt(11);
        //#8.  random values for total coffee dispensed
        totalCoffeeDispensed += cCoffeeDispensed;

        // #9.Generate timestamp here
        timestamp = System.currentTimeMillis();

        //#10. for days of week random**
        String[] days = new String[7];
        days[0] = "Sunday";
        days[1] = "Monday";
        days[2] = "Tuesday";
        days[3] = "Wednesday";
        days[4] = "Thursday";
        days[5] = "Friday";
        days[6] = "Saturday";
        String dayName = days[rd.nextInt(7)];

        //#11. for months random**
        String[] months = new String[12];
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";
        String monthName = months[rd.nextInt(12)]; //generate random month

        Date date = new Date();  //current date

        //#12. for current date generate random dates here
        SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
        String dateValue = dfDate.format(date);

        //#13. for time fetched from current time
        SimpleDateFormat dfTime = new SimpleDateFormat("HH:mm");
        String timeValue = dfTime.format(date);
        return new CoffeeData(cMilkLevel, MILK_CONTAINER_SIZE, cWaterLevel, WATER_CONTAINER_SIZE, cCoffeeLevel, COFFEE_CONTAINER_SIZE, cCoffeeDispensed, totalCoffeeDispensed, timestamp, dayName, monthName, dateValue, timeValue);
    }

    private static void onKaaStarted(long time) {
        if (time <= 0) {
            LOG.error("Wrong time is used. Please, check your configuration!");
            kaaClient.stop();
            System.exit(0);
        }

        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        CoffeeData cData = getCoffeeVMDataRand();
                        kaaClient.addLogRecord(new DataCollection(cData.getcMilkLevel(), cData.getMilkContainerSize(), cData.getcWaterLevel(), cData.getWaterContainerSize(), cData.getcCoffeeLevel(), cData.getCoffeeContainerSize(), cData.getcCoffeeDispensed(), cData.getTotalCoffeeDispensed(), cData.getTimeStamp(), cData.getDayName(), cData.getMonthName(), cData.getDateValue(), cData.getTimeValue()));
                        LOG.info("Current Milk Level: {}", cData.getcMilkLevel());
                        LOG.info("Milk Container Size: {}", cData.getMilkContainerSize());
                        LOG.info("Current Water Level: {}", cData.getcWaterLevel());
                        LOG.info("Water Container Size: {}", cData.getWaterContainerSize());
                        LOG.info("Current Coffee Level: {}", cData.getcCoffeeLevel());
                        LOG.info("Coffee Container Size: {}", cData.getCoffeeContainerSize());
                        LOG.info("Coffee Dispensed Between: {}", cData.getcCoffeeDispensed());
                        LOG.info("Total Coffee Dispensed: {}", cData.getTotalCoffeeDispensed());
                        LOG.info("Timestamp: {}", cData.getTimeStamp());
                        LOG.info("Day: {}", cData.getDayName());
                        LOG.info("Month: {}", cData.getMonthName());
                        LOG.info("Date: {}", cData.getDateValue());
                        LOG.info("Time: {}", cData.getTimeValue());
                    }
                }, 0, time, TimeUnit.MILLISECONDS);
    }

    private static void onChangedConfiguration(long time) {
        if (time == 0) {
            time = DEFAULT_START_DELAY;
        }
        scheduledFuture.cancel(false);

        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        CoffeeData cData = getCoffeeVMDataRand();
                        kaaClient.addLogRecord(new DataCollection(cData.getcMilkLevel(), cData.getMilkContainerSize(), cData.getcWaterLevel(), cData.getWaterContainerSize(), cData.getcCoffeeLevel(), cData.getCoffeeContainerSize(), cData.getcCoffeeDispensed(), cData.getTotalCoffeeDispensed(), cData.getTimeStamp(), cData.getDayName(), cData.getMonthName(), cData.getDateValue(), cData.getTimeValue()));
                        LOG.info("Current Milk Level: {}", cData.getcMilkLevel());
                        LOG.info("Milk Container Size: {}", cData.getMilkContainerSize());
                        LOG.info("Current Water Level: {}", cData.getcWaterLevel());
                        LOG.info("Water Container Size: {}", cData.getWaterContainerSize());
                        LOG.info("Current Coffee Level: {}", cData.getcCoffeeLevel());
                        LOG.info("Coffee Container Size: {}", cData.getCoffeeContainerSize());
                        LOG.info("Coffee Dispensed Between: {}", cData.getcCoffeeDispensed());
                        LOG.info("Total Coffee Dispensed: {}", cData.getTotalCoffeeDispensed());
                        LOG.info("Timestamp: {}", cData.getTimeStamp());
                        LOG.info("Day: {}", cData.getDayName());
                        LOG.info("Month: {}", cData.getMonthName());
                        LOG.info("Date: {}", cData.getDateValue());
                        LOG.info("Time: {}", cData.getTimeValue());
                    }
                }, 0, time, TimeUnit.MILLISECONDS);
    }

    private static class CoffeeMachineClientStateListener extends SimpleKaaClientStateListener {

        @Override
        public void onStarted() {
            super.onStarted();
            LOG.info("Kaa client started");
            Configuration configuration = kaaClient.getConfiguration();
            LOG.info("Default sample period: {}", configuration.getSamplePeriod());
            onKaaStarted(TimeUnit.SECONDS.toMillis(configuration.getSamplePeriod()));
        }

        @Override
        public void onStopped() {
            super.onStopped();
            LOG.info("Kaa client stopped");
        }
    }

    //model POJO class for Binding
    private static class CoffeeData {
        private int cMilkLevel, milkContainerSize, cWaterLevel, waterContainerSize, cCoffeeLevel, coffeeContainerSize, cCoffeeDispensed, totalCoffeeDispensed;
        private long timeStamp;
        private String dayName, monthName, dateValue, timeValue;

        public CoffeeData() {
            // Default Constructor
        }

        public CoffeeData(int cMilkLevel, int milkContainerSize, int cWaterLevel, int waterContainerSize, int cCoffeeLevel, int coffeeContainerSize, int cCoffeeDispensed, int totalCoffeeDispensed, long timeStamp, String dayName, String monthName, String dateValue, String timeValue) {
            this.cMilkLevel = cMilkLevel;
            this.milkContainerSize = milkContainerSize;
            this.cWaterLevel = cWaterLevel;
            this.waterContainerSize = waterContainerSize;
            this.cCoffeeLevel = cCoffeeLevel;
            this.coffeeContainerSize = coffeeContainerSize;
            this.cCoffeeDispensed = cCoffeeDispensed;
            this.totalCoffeeDispensed = totalCoffeeDispensed;
            this.timeStamp = timeStamp;
            this.dayName = dayName;
            this.monthName = monthName;
            this.dateValue = dateValue;
            this.timeValue = timeValue;
        }

        //getters
        public int getcMilkLevel() {
            return cMilkLevel;
        }

        public int getMilkContainerSize() {
            return milkContainerSize;
        }

        public int getcWaterLevel() {
            return cWaterLevel;
        }

        public int getWaterContainerSize() {
            return waterContainerSize;
        }

        public int getcCoffeeLevel() {
            return cCoffeeLevel;
        }

        public int getCoffeeContainerSize() {
            return coffeeContainerSize;
        }

        public int getcCoffeeDispensed() {
            return cCoffeeDispensed;
        }

        public int getTotalCoffeeDispensed() {
            return totalCoffeeDispensed;
        }

        public long getTimeStamp() {
            return timeStamp;
        }

        public String getDayName() {
            return dayName;
        }

        public String getMonthName() {
            return monthName;
        }

        public String getDateValue() {
            return dateValue;
        }

        public String getTimeValue() {
            return timeValue;
        }

        //setters
        public void setcMilkLevel(int cMilkLevel) {
            this.cMilkLevel = cMilkLevel;
        }

        public void setMilkContainerSize(int milkContainerSize) {
            this.milkContainerSize = milkContainerSize;
        }

        public void setcWaterLevel(int cWaterLevel) {
            this.cWaterLevel = cWaterLevel;
        }

        public void setWaterContainerSize(int waterContainerSize) {
            this.waterContainerSize = waterContainerSize;
        }

        public void setcCoffeeLevel(int cCoffeeLevel) {
            this.cCoffeeLevel = cCoffeeLevel;
        }

        public void setCoffeeContainerSize(int coffeeContainerSize) {
            this.coffeeContainerSize = coffeeContainerSize;
        }

        public void setcCoffeeDispensed(int cCoffeeDispensed) {
            this.cCoffeeDispensed = cCoffeeDispensed;
        }

        public void setTotalCoffeeDispensed(int totalCoffeeDispensed) {
            this.totalCoffeeDispensed = totalCoffeeDispensed;
        }

        public void setTimeStamp(long timeStamp) {
            this.timeStamp = timeStamp;
        }

        public void setDayName(String dayName) {
            this.dayName = dayName;
        }

        public void setMonthName(String monthName) {
            this.monthName = monthName;
        }

        public void setDateValue(String dateValue) {
            this.dateValue = dateValue;
        }

        public void setTimeValue(String timeValue) {
            this.timeValue = timeValue;
        }
    }
}